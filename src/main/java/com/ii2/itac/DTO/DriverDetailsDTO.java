package com.ii2.itac.DTO;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.ii2.itac.entity.Tags;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DriverDetailsDTO {

	Long driverId;

	Long deviceId;

	@NotNull(message = "Driver Name cannot be empty.")
	String driverName;

	String deviceName;

	String departmentName;
	
	@NotNull(message = "Department cannot be empty.")
	Integer departmentId;

	String employeeId;

	String hardwareKey;

	@NotNull(message = "Phone Number cannot be empty.")
	String phoneNumber;

	String email;

	@NotNull(message = "Address cannot be empty.")
	String address;

	@NotNull(message = "License Number cannot be empty.")
	String licenseNumber;

	String licenseClass;

	Date licenseExpirationDate;
	
	@NotNull(message = "Organization cannot be empty.")
	Long orgId;

	byte[] img;

	List<Tags> driverTags;

}
