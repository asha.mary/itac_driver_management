package com.ii2.itac.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseDTO {
	
	Long driverId;
	
	String message;

}
