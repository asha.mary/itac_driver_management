package com.ii2.itac.DTO;

import java.util.Date;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import com.ii2.itac.entity.Tags;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class DriverInput {

	Long driverId;

	@NotNull(message = "Driver Name cannot be empty.")
	String driverName;

	String employeeId;

	String hardwareKey;

	@NotNull(message = "Phone Number cannot be empty.")
	String phoneNumber;

	String email;

	@NotNull(message = "Address cannot be empty.")
	String address;

	@NotNull(message = "License Number cannot be empty.")
	String licenseNumber;

	String licenseClass;

	Date licenseExpirationDate;

	Boolean isDeleted = false;

	@NotNull(message = "Department cannot be empty.")
	Integer departmentId;

	Long deviceId;

	@NotNull(message = "Organization cannot be empty.")
	Long orgId;

	byte[] img;

	Set<Tags> driverTags;
}
