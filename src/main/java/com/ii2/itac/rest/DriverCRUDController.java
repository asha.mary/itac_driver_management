package com.ii2.itac.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ii2.itac.DTO.DriverDetailsDTO;
import com.ii2.itac.DTO.ResponseDTO;
import com.ii2.itac.exceptions.BadRequestAlertException;
import com.ii2.itac.exceptions.DataNotFoundException;
import com.ii2.itac.exceptions.NoActiveOrganizationException;
import com.ii2.itac.service.DriverCrudService;

@RestController
@RequestMapping("/api")
@Transactional
public class DriverCRUDController {
	
	@Autowired
	DriverCrudService driverCrudService;
	
	/**
	 * 
	 * @param userId
	 * @return
	 * @throws NoActiveOrganizationException
	 */
	@GetMapping("/list-drivers/{userId}")
	public ResponseEntity<List<DriverDetailsDTO>> getDriverListForUser(@PathVariable Long userId)
			throws NoActiveOrganizationException , DataNotFoundException{
		return new ResponseEntity<List<DriverDetailsDTO>>(driverCrudService.getDriverListForUser(userId),
				HttpStatus.OK);
	}
	
	/**
	 * 
	 * @param driver
	 * @return
	 * @throws BadRequestAlertException
	 */
	@PostMapping("/create-driver")
	public ResponseEntity<DriverDetailsDTO> createDriver(@Valid @RequestBody DriverDetailsDTO driver)
			throws BadRequestAlertException {
		return new ResponseEntity<DriverDetailsDTO>(driverCrudService.createDriver(driver), HttpStatus.OK);
	}
	
	/**
	 * 
	 * @param driver
	 * @return
	 * @throws BadRequestAlertException
	 */
	@PutMapping("/update-driver")
	public ResponseEntity<DriverDetailsDTO> updateDriver(@Valid @RequestBody DriverDetailsDTO driver)
			throws BadRequestAlertException {
		return new ResponseEntity<DriverDetailsDTO>(driverCrudService.updateDriver(driver), HttpStatus.OK);
	}
	
	/**
	 * 
	 * @param driverIds
	 * @return
	 */
	@PutMapping("/delete-driver")
	public ResponseEntity<List<ResponseDTO>> deleteDriver(@RequestBody List<Long> driverIds) {
		return new ResponseEntity<List<ResponseDTO>>(driverCrudService.deleteDriver(driverIds), HttpStatus.OK);
	}

}
