package com.ii2.itac.utilities;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ii2.itac.Repository.OrganizationRepository;
import com.ii2.itac.Repository.UsersOrgsRepository;
import com.ii2.itac.entity.Organization;
import com.ii2.itac.entity.UsersOrgs;

@Component
public class CommonUtils {

	@Autowired
	UsersOrgsRepository usersOrgsRepo;

	@Autowired
	OrganizationRepository orgRepo;

	/**
	 * function user to fetch active organization of a user
	 * 
	 * @param userId
	 * @return
	 */
	public List<Long> getActiveOrgsForUser(Long userId) {

		List<Long> activeOrgs = new ArrayList<>();
		Organization organization = new Organization();
		List<UsersOrgs> usersOrgs = usersOrgsRepo.findByuserId(userId);
		for (UsersOrgs userOrg : usersOrgs) {
			organization = orgRepo.findById(userOrg.getOrgId()).get();
			if (organization.getStatus().equalsIgnoreCase("Active")) {
				activeOrgs.add(organization.getOrgId());
			}

		}
		return activeOrgs;
	}
	
}
