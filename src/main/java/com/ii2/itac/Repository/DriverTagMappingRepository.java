package com.ii2.itac.Repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ii2.itac.entity.DriverTagMapping;

@Repository
public interface DriverTagMappingRepository extends JpaRepository<DriverTagMapping, Long>{
	
	@Transactional
	@Query("SELECT tagId FROM DriverTagMapping where driverId =:driverId")
	List<Integer> findByDriverId(@Param("driverId") Long driverId);

}
