package com.ii2.itac.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ii2.itac.entity.Tags;

@Repository
public interface TagsRepository extends JpaRepository<Tags, Integer>{
	
}
