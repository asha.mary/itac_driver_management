package com.ii2.itac.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ii2.itac.entity.Organization;

@Repository
public interface OrganizationRepository extends JpaRepository<Organization, Long>{

}
