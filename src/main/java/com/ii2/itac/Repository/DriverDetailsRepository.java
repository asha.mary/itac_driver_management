package com.ii2.itac.Repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ii2.itac.entity.DriverDetails;

@Repository
public interface DriverDetailsRepository extends JpaRepository<DriverDetails, Long>{

	@Transactional
	@Query("FROM DriverDetails where orgId in :orgIds and isDeleted = false")
	List<DriverDetails> findByOrgs(@Param("orgIds") List<Long> orgIds);
	
	@Transactional
	@Query("FROM DriverDetails where driverId =:driverId and isDeleted = false")
	DriverDetails findByDriverId(@Param("driverId") Long driverId);
	
	@Transactional
	@Query("FROM DriverDetails where deviceId =:deviceId")
	DriverDetails findByDeviceId(@Param("deviceId") Long deviceId);
	
	@Modifying
	@Query("UPDATE DriverDetails SET isDeleted = true ,deviceId = NULL where driverId =:driverId")
	void deleteBydriverId(@Param("driverId") Long driverId);
}
