package com.ii2.itac.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ii2.itac.entity.UsersOrgs;

@Repository
public interface UsersOrgsRepository extends JpaRepository<UsersOrgs, Long>{
	@Query("FROM UsersOrgs where userId =:userId")
	List<UsersOrgs> findByuserId(@Param("userId") Long userId);
}
