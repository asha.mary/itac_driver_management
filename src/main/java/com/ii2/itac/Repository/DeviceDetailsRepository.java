package com.ii2.itac.Repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ii2.itac.entity.DeviceDetails;

@Repository
public interface DeviceDetailsRepository extends JpaRepository<DeviceDetails, Long>{

	@Transactional
	@Query("FROM DeviceDetails where orgId in :orgIds and isDeleted = false")
	List<DeviceDetails> findByOrgIds(@Param("orgIds") List<Long> orgIds);
}
