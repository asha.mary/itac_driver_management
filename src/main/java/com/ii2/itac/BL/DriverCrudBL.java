package com.ii2.itac.BL;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ii2.itac.DTO.DriverDetailsDTO;
import com.ii2.itac.DTO.ResponseDTO;
import com.ii2.itac.Repository.DepartmentRepository;
import com.ii2.itac.Repository.DeviceDetailsRepository;
import com.ii2.itac.Repository.DriverDetailsRepository;
import com.ii2.itac.Repository.DriverTagMappingRepository;
import com.ii2.itac.Repository.TagsRepository;
import com.ii2.itac.entity.Department;
import com.ii2.itac.entity.DeviceDetails;
import com.ii2.itac.entity.DriverDetails;
import com.ii2.itac.entity.Tags;
import com.ii2.itac.exceptions.BadRequestAlertException;
import com.ii2.itac.exceptions.DataNotFoundException;
import com.ii2.itac.exceptions.NoActiveOrganizationException;
import com.ii2.itac.utilities.CommonUtils;
import com.itac.utilities.LoggerStackTraceUtil;

@Component
public class DriverCrudBL {

	@Autowired
	DriverDetailsRepository driverRepo;

	@Autowired
	DeviceDetailsRepository deviceRepo;

	@Autowired
	DepartmentRepository departmentRepo;

	@Autowired
	DriverTagMappingRepository driverTagMapRepo;

	@Autowired
	TagsRepository tagRepo;

	@Autowired
	CommonUtils commonUtils;

	ModelMapper mapper = new ModelMapper();

	/**
	 * 
	 * @param userId
	 * @return
	 * @throws NoActiveOrganizationException
	 * @throws DataNotFoundException
	 */
	public List<DriverDetailsDTO> getDriverListForUser(Long userId)
			throws NoActiveOrganizationException, DataNotFoundException {

		List<DriverDetailsDTO> driverList = null;
		try {
			List<Long> activeOrgs = commonUtils.getActiveOrgsForUser(userId);
			if (activeOrgs.isEmpty()) {
				throw new NoActiveOrganizationException("User does not have any active organization");
			}
			driverList = getDriverListForOrgs(activeOrgs);
			if (driverList.isEmpty()) {
				throw new DataNotFoundException("No data found");
			}
		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
			throw e;
		}

		return driverList;
	}

	/**
	 * 
	 * @param driver
	 * @return
	 * @throws BadRequestAlertException
	 */
	public DriverDetailsDTO createDriver(DriverDetailsDTO input) throws BadRequestAlertException {
		DriverDetailsDTO result;
		DriverDetails driverDetails;
		DriverDetails driverInput;
		Long deviceId = input.getDeviceId();
		
		try {
			if (input.getDriverId() != null) {
				throw new BadRequestAlertException("A new driver cannot already have an ID");
			} else {
				if (deviceId != null) {
					DriverDetails driver = driverRepo.findByDeviceId(deviceId);
					if (driver != null ) {
						throw new BadRequestAlertException("Device alreday assigned to another driver");
					}
				}
				driverInput = mapper.map(input, DriverDetails.class);
				driverDetails = driverRepo.save(driverInput);
				result = getDriverDetailsDTO(driverDetails);
			}
		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
			throw e;
		}
		return result;
	}

	/**
	 * 
	 * @param driver
	 * @return
	 * @throws BadRequestAlertException
	 */

	public DriverDetailsDTO updateDriver(DriverDetailsDTO input) throws BadRequestAlertException {
		DriverDetailsDTO result;
		DriverDetails driverDetails;
		DriverDetails driverInput;
		Long deviceId = input.getDeviceId();
		try {
			Long driverId = input.getDriverId();
			if (driverId == null) {
				throw new BadRequestAlertException("Driver with id: " + driverId + " does not exist");
			} else {
				if (deviceId != null) {
					DriverDetails driver = driverRepo.findByDeviceId(deviceId);
					if (driver != null && (driver.getDriverId() != driverId)) {
						throw new BadRequestAlertException("Device alreday assigned to another driver");
					}
				}
				driverInput = mapper.map(input, DriverDetails.class);
				driverDetails = driverRepo.save(driverInput);
				result = getDriverDetailsDTO(driverDetails);
			}
		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
			throw e;
		}
		return result;
	}

	/**
	 * 
	 * @param driverId
	 * @throws BadRequestAlertException
	 */
	public List<ResponseDTO> deleteDriver(List<Long> driverIds){
		List<ResponseDTO> response = new ArrayList<>();
		ResponseDTO resObj;
		try {
			for(Long driverId: driverIds) {
				DriverDetails driver = driverRepo.findByDriverId(driverId);
				if (driver != null) {
					driverRepo.deleteBydriverId(driverId);
					resObj = new ResponseDTO();
					resObj.setDriverId(driverId);
					resObj.setMessage("Driver with id:"+ driverId + " deleted successfully");
				} else {
					resObj = new ResponseDTO();
					resObj.setDriverId(driverId);
					resObj.setMessage("Driver with id:"+ driverId + " doesn't exist");
				}
				response.add(resObj);
			}	

		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);			
		}
   return response;
	}

	/**
	 * function to get driver details for a user
	 * 
	 * @param orgIds
	 * @return
	 */
	public List<DriverDetailsDTO> getDriverListForOrgs(List<Long> orgIds) {
		List<DriverDetailsDTO> driverList = new ArrayList<>();
		List<DriverDetails> driverDetailsList = null;
		DriverDetailsDTO dtoObj;
		driverDetailsList = driverRepo.findByOrgs(orgIds);
		for (DriverDetails driver : driverDetailsList) {
			dtoObj = new DriverDetailsDTO();
			dtoObj = getDriverDetailsDTO(driver);
			driverList.add(dtoObj);

		}
		return driverList;
	}

	/**
	 * function used to get driverDetailsDTO from driverDetails
	 * 
	 * @param driver
	 * @return
	 */
	public DriverDetailsDTO getDriverDetailsDTO(DriverDetails driver) {
		DriverDetailsDTO dtoObj = new DriverDetailsDTO();
		try {
			Long driverId = driver.getDriverId();
			Long deviceId = driver.getDeviceId();
			Integer departmentId = driver.getDepartmentId();
			dtoObj = mapper.map(driver, DriverDetailsDTO.class);
			if (deviceId != null) {
				Optional<DeviceDetails> device = deviceRepo.findById(deviceId);
				if (device.isPresent()) {
					dtoObj.setDeviceName(device.get().getDeviceName());
					dtoObj.setDeviceId(deviceId);
				}
			}
			if (departmentId != null) {
				Optional<Department> dept = departmentRepo.findById(departmentId);
				if (dept.isPresent()) {
					dtoObj.setDepartmentName(dept.get().getDepartmentName());
				}
			}

			List<Integer> tagIds = driverTagMapRepo.findByDriverId(driverId);
			if (tagIds.isEmpty()) {
				List<Tags> tags = tagRepo.findAllById(tagIds);
				dtoObj.setDriverTags(tags);
			}

		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
		}
		return dtoObj;
	}
}
