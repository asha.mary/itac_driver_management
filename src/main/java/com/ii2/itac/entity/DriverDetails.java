package com.ii2.itac.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "driver_details")
@Getter
@Setter
public class DriverDetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "driver_id")
	private Long driverId;	

	@Column(name = "driver_name", nullable = false)
	private String driverName;
	
	private String employeeId;
	
	@Column(name = "hardware_key")
	private String hardwareKey;
	
	@Column(name = "phone_number", nullable = false)
	private String phoneNumber;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "address", nullable = false)
	private String address;
	
	@Column(name = "license_number", nullable = false)
	private String licenseNumber;
	
	@Column(name = "license_class")
	private String licenseClass;
	
	private Date licenseExpirationDate;
	
	@Column(name = "is_deleted", columnDefinition="tinyint(1) default 0", nullable = false)
	private Boolean isDeleted = false;	
	
	@Column(name = "department_id",nullable = false)
	private Integer departmentId;
	
	@Column(name = "device_id")
	private Long deviceId;
	
	@Column(name = "org_id", nullable = false)
	private Long orgId;
	
	@Lob
	@Column(name = "img")
	private byte[] img;
	
	@ManyToMany
	@JoinTable(name = "driver_tag_mapping", joinColumns = @JoinColumn(name = "driver_id"), inverseJoinColumns = @JoinColumn(name = "tag_id"))
	Set<Tags> driverTags;

}
