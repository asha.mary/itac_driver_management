package com.ii2.itac.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ii2.itac.BL.DriverCrudBL;
import com.ii2.itac.DTO.DriverDetailsDTO;
import com.ii2.itac.DTO.ResponseDTO;
import com.ii2.itac.exceptions.BadRequestAlertException;
import com.ii2.itac.exceptions.DataNotFoundException;
import com.ii2.itac.exceptions.NoActiveOrganizationException;
import com.ii2.itac.service.DriverCrudService;

@Component
public class DriverCrudServiceImpl implements DriverCrudService{
	
	@Autowired
	DriverCrudBL driverCrudBL;

	@Override
	public List<DriverDetailsDTO> getDriverListForUser(Long userId) throws NoActiveOrganizationException , DataNotFoundException{
		return driverCrudBL.getDriverListForUser(userId);
	}

	@Override
	public DriverDetailsDTO createDriver(DriverDetailsDTO driver) throws BadRequestAlertException {
		return driverCrudBL.createDriver(driver);
	}

	@Override
	public DriverDetailsDTO updateDriver(DriverDetailsDTO driver) throws BadRequestAlertException {
		return driverCrudBL.updateDriver(driver);
	}

	@Override
	public List<ResponseDTO> deleteDriver(List<Long> driverIds){
		return driverCrudBL.deleteDriver(driverIds);
	}

}
