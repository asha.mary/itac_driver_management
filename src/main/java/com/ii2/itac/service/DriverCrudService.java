package com.ii2.itac.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ii2.itac.DTO.DriverDetailsDTO;
import com.ii2.itac.DTO.ResponseDTO;
import com.ii2.itac.exceptions.BadRequestAlertException;
import com.ii2.itac.exceptions.DataNotFoundException;
import com.ii2.itac.exceptions.NoActiveOrganizationException;

@Service
public interface DriverCrudService {

	List<DriverDetailsDTO> getDriverListForUser(Long userId) throws NoActiveOrganizationException, DataNotFoundException;

	DriverDetailsDTO createDriver(DriverDetailsDTO driver) throws BadRequestAlertException;

	DriverDetailsDTO updateDriver(DriverDetailsDTO driver) throws BadRequestAlertException;

	List<ResponseDTO> deleteDriver(List<Long> driverIds);

}
