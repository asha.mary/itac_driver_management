package com.ii2.itac.BL;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.ii2.itac.DTO.DriverDetailsDTO;
import com.ii2.itac.DTO.ResponseDTO;
import com.ii2.itac.exceptions.BadRequestAlertException;
import com.ii2.itac.exceptions.DataNotFoundException;
import com.ii2.itac.exceptions.NoActiveOrganizationException;
import com.itac.utilities.LoggerStackTraceUtil;

@SpringBootTest
class DriverCrudBLTest {

	@Autowired
	DriverCrudBL blObj;

	@Autowired
	DriverDetailsDTO driver;

	/**
	 * list driver for no data
	 */
	// @Test
	void getDriverListForNoDataTest() {
		Long userId = 1L;
		Boolean check = false;
		try {
			blObj.getDriverListForUser(userId);
		} catch (DataNotFoundException e) {
			check = true;
		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
		}
		assertThat(check, is(true));
	}

	/**
	 * list driver for no active organization
	 */
	// @Test
	void getDriverListForNoActiveOrgTest() {
		Long userId = 2L;
		Boolean check = false;
		try {
			blObj.getDriverListForUser(userId);
		} catch (NoActiveOrganizationException e) {
			check = true;
		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
		}
		assertThat(check, is(true));
	}

	/**
	 * create driver with id
	 */
	// @Test
	void createDriverWithIdTest() {
		Boolean check = false;
		driver = new DriverDetailsDTO();
		driver.setDriverId(1L);
		driver.setDriverName("driver 1");
		driver.setLicenseNumber("765716764");
		driver.setOrgId(1L);
		driver.setPhoneNumber("9876453210");
		driver.setAddress("Flat no:xx, XYZ appartment");
		try {
			blObj.createDriver(driver);
		} catch (BadRequestAlertException e) {
			check = true;
		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
		}
		assertThat(check, is(true));
	}

	/**
	 * create driver
	 */
	// @Test
	void createDriverTest() {

		driver.setDriverName("driver 1");
		driver.setLicenseNumber("765716764");
		driver.setOrgId(1L);
		driver.setPhoneNumber("9876453210");
		driver.setAddress("Flat no:xx, XYZ appartment");
		try {
			assertThat(blObj.createDriver(driver) != null, is(true));
		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
		}
	}

	/**
	 * list driver
	 */
	// @Test
	void getDriverListTest() {
		Long userId = 1L;
		try {
			assertThat(blObj.getDriverListForUser(userId).size() == 1, is(true));

		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
		}
	}

	/**
	 * update driver with no id
	 */
	// @Test
	void updateDriverTest() {
		Boolean check = false;
		driver = new DriverDetailsDTO();
		driver.setDriverName("driver 1");
		driver.setLicenseNumber("765716764");
		driver.setOrgId(1L);
		driver.setPhoneNumber("9876453210");
		driver.setAddress("Flat no:12/B, XYZ appartment");
		try {
			blObj.updateDriver(driver);
		} catch (BadRequestAlertException e) {
			check = true;
		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
		}
		assertThat(check, is(true));
	}

	/**
	 * update driver with already assigned device id
	 */
	// @Test
	void updateDriverWithAssignedDeviceTest() {
		Boolean check = false;
		driver = new DriverDetailsDTO();
		driver.setDriverId(1L);
		driver.setDeviceId(2L);
		driver.setDriverName("driver 1");
		driver.setLicenseNumber("765716764");
		driver.setOrgId(1L);
		driver.setPhoneNumber("9876453210");
		driver.setAddress("Flat no:12/B, XYZ appartment");
		try {
			blObj.updateDriver(driver);
		} catch (BadRequestAlertException e) {
			check = true;
		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
		}
		assertThat(check, is(true));
	}

	/**
	 * delete driver with invalid id
	 */
	// @Test
	void deleteDriverTest() {
		List<Long> driverIds = new ArrayList<>();
		driverIds.add(1L);
		List<ResponseDTO> result = null;
		try {
			result = blObj.deleteDriver(driverIds);
		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
		}
		assertThat(result.get(0).getMessage(), is("Driver with id:1 doesn't exist"));
	}

	/**
	 * create driver with already assigned device
	 */
	@Test
	void createDriverWithAssignedDeviceTest() {

		driver.setDriverName("driver 1");
		driver.setDeviceId(1L);
		driver.setDepartmentId(1);
		driver.setLicenseNumber("765716764");
		driver.setOrgId(1L);
		driver.setPhoneNumber("9876453210");
		driver.setAddress("Flat no:xx, XYZ appartment");
		try {
			assertThat(blObj.createDriver(driver) != null, is(true));
		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
		}
	}
}
